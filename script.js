'use strict'

const eyes = document.querySelectorAll(".icon-password");
const form = document.querySelector('.password-form');

form.addEventListener('submit', (event) => {
    let password = event.target.querySelector('.password');
    let passwordConfirm = event.target.querySelector('.password_confirm');
    if (password.value === passwordConfirm.value) {
        alert('You are welcome');
    } else {
        alert('Потрібно ввести однакові значення');
    }
})

eyes.forEach((element) => {
    element.addEventListener('click', (event) => {
        toggleEye(event.target);
        toggleInputPasswordType(event.target.parentNode.querySelector('input'));
    })
})

function toggleEye(eye) {
    if (eye.classList.contains('fa-eye')) {
        eye.classList.remove('fa-eye');
        eye.classList.add('fa-eye-slash');
    } else {
        eye.classList.remove('fa-eye-slash');
        eye.classList.add('fa-eye');
    }
}

function toggleInputPasswordType(input) {
    if (input.getAttribute('type') === 'password') {
        input.setAttribute('type', 'text');
    } else {
        input.setAttribute('type', 'password');
    }
}
